#!/bin/bash
# Install OpenCV for TK1 Board

# Jetson TK1 development kit
# Download the opencv_extras, opencv and glib from repository
# If you are installing the opencv testdata, ie
# OPENCV_TEST_DATA_PATH=../opencv_extra/testdata

# Clone OpenCV
echo "Cloning OpenCV from https://github.com/opencv/opencv.git..."
git clone https://github.com/opencv/opencv.git
cd opencv
git checkout -b v3.3.1 3.3.1

# Clone OpenCV_Extra
echo "Cloning OpenCV_Extra from https://github.com/opencv/opencv_extra.git..."
cd ..
git clone https://github.com/opencv/opencv_extra.git
cd opencv_extra
git checkout -b v3.3.1 3.3.1

# Repository setup
sudo apt-add-repository universe
sudo apt-get update

#Install dependencies 
sudo apt-get install \
    libglew-dev \
    libtiff5-dev \
    zlib1g-dev \
    libjpeg-dev \
    libpng12-dev \
    libjasper-dev \
    libavcodec-dev \
    libavformat-dev \
    libavutil-dev \
    libpostproc-dev \
    libswscale-dev \
    libeigen3-dev \
    libtbb-dev \
    libgtk2.0-dev \
    pkg-config

#Python support 
sudo apt-get install python-dev python-numpy python-py python-pytest
sudo apt-get install python3-dev python3-numpy python3-py python3-pytest

# GStreamer support
sudo apt-get install -y libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev

#Ready to build 
cd ..
mkdir build_opencv
cd build_opencv

#Configure 
cmake -DOPENCV_TEST_DATA_PATH=../opencv_extra/testdata \
../opencv

make -j6
sudo make install
#echo "Install OpenCV's success!"

#Download dlib
cd ..
echo "dbib's installing ..."
git clone https://github.com/davisking/dlib

echo "Install dlib's success!"


