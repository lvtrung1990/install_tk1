#!/bin/sh
# Fresh install for CUDA 6.5 on Jetson TK1 for Linux for Tegra (L4T) 21.1
# CUDA 6.5 REQUIRES L4T 21.1 !!!

mkdir cuda65
cd cuda65

sudo apt-add-repository universe
sudo apt-get update
# This is for L4T r21.1 ; Update for your L4T i.e. r21.3
wget http://developer.download.nvidia.com/compute/cuda/6_5/rel/installers/cuda-repo-l4t-r21.1-6-5-prod_6.5-14_armhf.deb

# Install the CUDA repo metadata that you downloaded 
# This is for L4T 21.1 ; Update for your L4T i.e. 21.3
sudo dpkg -i cuda-repo-l4t-r21.1-6-5-prod_6.5-14_armhf.deb
# Download & install the actual CUDA Toolkit including the OpenGL toolkit from NVIDIA. 
sudo apt-get install cuda-toolkit-6-5 -y

# Add yourself to the "video" group to allow access to the GPU
sudo usermod -a -G video $USER
#Add the 32-bit CUDA paths to your .bashrc login script, and start using it in your current console:

echo "# Add CUDA bin & library paths:" >> ~/.bashrc
echo "export PATH=/usr/local/cuda-6.5/bin:$PATH" >> ~/.bashrc
echo "export LD_LIBRARY_PATH=/usr/local/cuda-6.5/lib:$LD_LIBRARY_PATH" >> ~/.bashrc
source ~/.bashrc

echo "Success!"

# Build Samples (optional)
# cd /usr/local/cuda-6.5/bin
# ./cuda-install-samples-6.5.sh ~/
# Switch to CUDA directory in ~/
# make
