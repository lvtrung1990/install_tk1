# build OpenCV and Glib on jetson TK1 kit (native compilation)
 
These scripts build OpenCV version 3.3.1 for the NVIDIA Jetson TK1 Development Kit.
OpenCV is a rich environment which can be configured in many different ways. Please read the notes below for other important points before installing.


# Go into the your folder that contains packets. 

# To install CUDA6.5 for Jetson TK1 Development (Optional)
$ cd install_tk1
$ ./installCUDA65.sh

# To intall OpenCV and Glib
$ ./installOpenCV_dlib.sh

# Build and run dlib examples
$ cd dlib/examples/
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .

#Build exampleOpenCV
$ cd example/
$ cmake .
$ make




